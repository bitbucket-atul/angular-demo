import { Component, OnInit } from '@angular/core';
import { BiodataService } from '../../biodata.service';

@Component({
  selector: 'app-childabout',
  templateUrl: './childabout.component.html',
  styleUrls: ['./childabout.component.styl']
})
export class ChildaboutComponent implements OnInit {

public data;
public showBiodata=[];

  constructor ( private biodataService : BiodataService) { }

  ngOnInit() {
   this.data = this.biodataService.howserviceworks();
   this.showBiodata = this.biodataService.biodata();
  }

}
