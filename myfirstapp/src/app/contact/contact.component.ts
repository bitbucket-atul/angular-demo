import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.styl']
})
export class ContactComponent implements OnInit {

  constructor() { 
    
  }
  propobject = {
    carname : 'honda',
    carcolor : 'white',
    carmodel : 'xuv'
  }
  // type MyArrayType = Array<{id: number, text: string}>;

 public student = [{
    sId: 1001, 
    sName: "chandra shekhar",
    sAddress: "Hyderabad",
    marks: [65, 85, 68, 75, 72, 97],
    status:"pass",
  }]
  ngOnInit() {
  }

  people: any[] = [
    {
      "name": "Douglas  Pace",
      "age": 35
    },
    {
      "name": "Mcleod  Mueller",
      "age": 32
    },
    {
      "name": "Day  Meyers",
      "age": 21
    },
    {
      "name": "Aguirre  Ellis",
      "age": 34
    },
    {
      "name": "Cook  Tyson",
      "age": 32
    }
  ];
  getColor(country) { 
    switch (country) {
      case 'UK':
        return 'green';
      case 'USA':
        return 'blue';
      case 'HK':
        return 'red';
    }
  }
}
