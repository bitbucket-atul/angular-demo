import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { ContactComponent } from './contact/contact.component';
import { ProfileComponent } from './profile/profile.component';
import { HeaderComponent } from './header/header.component';
import { ChildaboutComponent } from './about/childabout/childabout.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';

import { ServisedateService } from './servisedate.service';
import {BiodataService} from './biodata.service';
import { GetdataComponent } from './getdata/getdata.component';

const headermenu: Routes = [
  
  { path: "", redirectTo: "home", pathMatch: "full" },
  { path: 'home', component: HomeComponent },
  { path: 'aboutus', component: AboutComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'contactus', component: ContactComponent },
  { path: 'child', component: ChildaboutComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ContactComponent,
    ProfileComponent,
    HeaderComponent,
    ChildaboutComponent,
    FooterComponent,
    HomeComponent,
    GetdataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(
    headermenu)   
  ],
  providers : [ServisedateService,BiodataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
