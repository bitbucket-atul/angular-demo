import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IBiodata }  from './biodata';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServisedateService {

  constructor( private http: HttpClient) { }
  servicedata(): Observable<IBiodata[]> {
    return this.http.get<IBiodata[]>('https://jsonplaceholder.typicode.com/posts');
  }
}
