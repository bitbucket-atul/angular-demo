import { TestBed } from '@angular/core/testing';

import { ServisedateService } from './servisedate.service';

describe('ServisedateService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ServisedateService = TestBed.get(ServisedateService);
    expect(service).toBeTruthy();
  });
});
