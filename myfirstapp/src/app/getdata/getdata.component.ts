import { Component, OnInit } from '@angular/core';
import { ServisedateService } from '../servisedate.service';

@Component({
  selector: 'app-getdata',
  templateUrl: './getdata.component.html',
  styleUrls: ['./getdata.component.styl']
})
export class GetdataComponent implements OnInit {
  public data ;
  public showBiodata = [];

  constructor( private servicedata: ServisedateService) { }

  ngOnInit() {
     this.servicedata.servicedata().subscribe(data => this.showBiodata = data) ;
  }

}
